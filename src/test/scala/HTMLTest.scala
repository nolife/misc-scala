import org.junit.Test
import org.scalatest.Suite
import com.coldcore.misc.scala.HTML._

class HTMLTest extends Suite {

  @Test
  def testPlainToHTML(): Unit = {
    val inputPlain = "   this # is \r\n next # line <p>T_T</p> \n\n\n   t  a  g   \n  \n'Q_Q\" &  #?!"
    assertResult(" &nbsp; this # is <br> next # line &lt;p&gt;T_T&lt;/p&gt; <br><br><br> &nbsp; t &nbsp;a &nbsp;g &nbsp; <br> &nbsp;<br>'Q_Q&quot; &amp; &nbsp;#?!") { plainToHTML(inputPlain) }
    assertResult(" &nbsp; this &hash; is <br> next &hash; line &lt;p&gt;T_T&lt;/p&gt; <br><br><br> &nbsp; t &nbsp;a &nbsp;g &nbsp; <br> &nbsp;<br>'Q_Q&quot; &amp; &nbsp;&hash;?!") { plainToHTML(inputPlain, Map("#" -> "&hash;")) }
    assertResult(" &nbsp; this # is \n next # line &lt;p&gt;T_T&lt;/p&gt; \n\n\n &nbsp; t &nbsp;a &nbsp;g &nbsp; \n &nbsp;\n'Q_Q&quot; &amp; &nbsp;#?!") { plainToHTML(inputPlain, Map("<br>" -> "\n")) }
    assertResult("this # is<br>next # line &lt;p&gt;T_T&lt;/p&gt;<br>t &nbsp;a &nbsp;g<br>'Q_Q&quot; &amp; &nbsp;#?!") { plainToHTML_lb(inputPlain) }
  }

  @Test
  def testPlainToHTML_lb(): Unit = {
    val inputPlain = "   \r\n\n text  \n\n \r\n more text \n more here!\n\n\n\n\r\n??"
    assertResult("text<br>more text<br>more here!<br>??") { plainToHTML_lb(inputPlain) }
  }

  @Test
  def testPlainToHTML_dlb(): Unit = {
    val inputPlain = "   \r\n\n text  \n\n \r\n more text \n more here!\n\n\n\n\r\n??"
    assertResult("text<br><br>more text<br>more here!<br><br>??") { plainToHTML_dlb(inputPlain) }
  }

}