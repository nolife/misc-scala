import org.junit.Test
import org.scalatest.Suite
import com.coldcore.misc.scala.StringOp._

class StringOpTest extends Suite {

  @Test
  def testParametrize(): Unit = {
    val input = "The {amount} on {date}"
    assertResult("The 100 on 200") { parametrize(input, "amount:100", "date:200") }
    assertResult("The 100 on {date}") { parametrize(input, "amount:100") }
    assertResult("The 100 on 200") { parametrizeText(input, List("amount:100", "date:200")) }
    assertResult("The 100 on {date}") { parametrizeText(input, List("amount:100")) }
    assertResult(input) { parametrizeText(input, List()) }
    assertResult(input) { parametrize(input) }
    assertResult(input) { parametrize(input, "my:void") }
  }

  @Test
  def testParseAndTrim(): Unit = {
    assertResult("the line\nwith breaks\nand trailings") { trimm(" the line\n   with breaks  \r\n\r\n  and trailings  ") }
    assertResult("ALL UPPER") { trimu(" All upper ") }
    assertResult("all lower") { triml(" All LOWER ") }
    assertResult("some") { trimCut(" some cutted ... ooouno ", 5) }
    assertResult("some c") { trimCut(" some cutted ... ooouno ", 6) }
    assertResult(true) { isTrue(" 1 ") }
    assertResult(false) { isTrue(" 0 ") }
    assertResult(false) { isTrue(" 0 ", true) }
    assertResult(false) { isTrue(" go ") }
    assertResult(true) { isTrue(" go ", true) }
    assertResult(false) { isTrue(" go ", false) }
  }

  @Test
  def testParseCSV(): Unit = {
    val input1 = "  key1=val1; key2 = val2 ,,  ;, key3 = val3 , key4 = val4 "
    val input2 = "  key1=val1; key2 = val2 ,, val2n ; key3 = val3,,,val3n=X; key4 = val4 "
    assertResult(List("key1=val1", "key2 = val2", "key3 = val3", "key4 = val4")) { parseCSV(input1) }
    assertResult(List("key1", "val1", "key2", "val2", "val2n", "key3", "val3", "val3n", "X", "key4", "val4")) { parseCSV(input2, "=,;") }
    assertResult(Map("key1" -> "val1", "key2" -> "val2", "key3" -> "val3", "key4" -> "val4")) { parseCSVMap(input1) }
    assertResult(Map("key1" -> "val1", "key2" -> "val2 ,, val2n", "key3" -> "val3,,,val3n=X", "key4" -> "val4")) { parseCSVMap(input2, ";") }
    assertResult(Map.empty) { parseCSVMap("") }
    assertResult(Map("key" -> "")) { parseCSVMap("key=") }
  }

  @Test
  def testToX(): Unit = {
    assertResult(8000) { toX100("80") }
    assertResult("80.00") { toX100(8000) }
    assertResult(7090) { toX100("70,9") }
    assertResult("70.90") { toX100(7090) }
    assertResult(6095) { toX100("60.95") }
    assertResult("60.95") { toX100(6095) }
    assertResult(5095) { toX100("50.959") }
    assertResult("50.95") { toX100(5095) }
    assertResult(0) { toX100("0") }
    assertResult("0.00") { toX100(0) }
    assertResult(40) { toX100(".4") }
    assertResult("0.40") { toX100(40) }
    assertResult(0) { toX100("") }
    assertResult("0.00") { toX100(0) }
    assertResult(-8000) { toX100("-80") }
    assertResult("-80.00") { toX100(-8000) }
    assertResult(-40) { toX100("-0.4") }
    assertResult("-0.40") { toX100(-40) }
  }

  @Test
  def testToNumber(): Unit = {
    assertResult(10) { toLong("10") }
    assertResult(10) { toLong(" 10 ") }
    assertResult(10) { toLong(" x ", 10) }
    assertResult(0) { toLong(" x ") }
    assertResult(10) { toInt("10") }
    assertResult(10) { toInt(" 10 ") }
    assertResult(10) { toInt(" x ", 10) }
    assertResult(0) { toInt(" x ") }
  }

}