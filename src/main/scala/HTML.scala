package com.coldcore.misc.scala

import com.coldcore.misc.scala.StringOp._

object HTML {

  /** Convert a plain text into HTML.
      Return an HTML with basic and/or additional symbols converted into HTML entities. */
  def plainToHTML(text: String, rep: Map[String,String] = Map.empty): String = {
    val x = List(("&", "&amp;"), ("<", "&lt;"), (">", "&gt;"), ("\n", "<br>"), ("\r", ""), ("\"", "&quot;"), ("  ", " &nbsp;")) //basic symbols
    chainReplace(text, x ::: rep.toList)
  }

  /** Strip line breakes in a text to a single or double line break. */
  private def compact_dlb(text: String): String = {
    val x = text replaceAll("\r", "") split "\n\n" map(_.trim) filter(_.length > 0) mkString "\r\r"
    chainReplace(trimm(x), List(("\r\n", "\r"), ("\n\r", "\r"), ("\r\r", "\n\n")))
  }

  /** Convert a plain text into HTML stripping line breaks to a single and changing basic symbols into entities.
      Use to format a free hand multiline text entered by a user. */
  def plainToHTML_lb(text: String): String =
    plainToHTML(trimm(text))

  /** Same as 'plain-to-html-lb' but allow double line breaks. */
  def plainToHTML_dlb(text: String): String =
    plainToHTML(compact_dlb(text))

}