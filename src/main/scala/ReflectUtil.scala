package com.coldcore.misc.scala

object ReflectUtil {
  import java.lang.reflect.{Field, Method}
  import java.lang.annotation.Annotation
  import reflect.{ClassTag, classTag}

  def invokeMethod(o: AnyRef, method: String, args: Any*): Any = {
    val a = args.asInstanceOf[Seq[AnyRef]]
    val m =
      try { o.getClass.getMethod(method, a.map(_.getClass): _*) }
      catch {
        case e: NoSuchMethodException =>
          methodByName(o.getClass, method).getOrElse(
            throw new NoSuchMethodException("Method "+method+" not found in "+o.getClass))
      }
    m.invoke(o, a: _*)
  }

  private def filterClassFields(c: Class[_])(f: Field => Boolean): List[Field] =
    c.getDeclaredFields.filter(f).toList ::: (if (c.getSuperclass != null) filterClassFields(c.getSuperclass)(f) else Nil)

  private def filterClassMethods(c: Class[_])(f: Method => Boolean): List[Method] =
    c.getMethods.filter(f).toList

  def fieldsByAnnotation[A <: Annotation : ClassTag](c: Class[_]): List[Field] =
    filterClassFields(c) { _.isAnnotationPresent(classTag[A].runtimeClass.asInstanceOf[Class[A]]) }

  def fieldByAnnotation[A <: Annotation : ClassTag](c: Class[_]): Option[Field] =
    fieldsByAnnotation[A](c).headOption

  def fieldByName(c: Class[_], n: String): Option[Field] =
    filterClassFields(c) { _.getName == n }.headOption

  def methodsByAnnotation[A <: Annotation : ClassTag](c: Class[_]): List[Method] =
    filterClassMethods(c) { _.isAnnotationPresent(classTag[A].runtimeClass.asInstanceOf[Class[A]]) }

  def methodByAnnotation[A <: Annotation : ClassTag](c: Class[_]): Option[Method] =
    methodsByAnnotation[A](c).headOption

  def methodByName(c: Class[_], n: String): Option[Method] =
    filterClassMethods(c) { _.getName == n }.headOption

  private val getter = (s: String) => "get"+s.substring(0,1).toUpperCase+s.substring(1)
  private val setter = (s: String) => "set"+s.substring(0,1).toUpperCase+s.substring(1)

  def resetField(o: AnyRef, f: Field, v: Any): Any = resetField(o, f.getName, v)
  def resetField(o: AnyRef, fname: String, v: Any): Any = invokeMethod(o, setter(fname), v)

  def fieldValue(o: AnyRef, f: Field): Any = fieldValue(o, f.getName)
  def fieldValue(o: AnyRef, fname: String): Any = invokeMethod(o, getter(fname))
}
