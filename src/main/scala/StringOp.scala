package com.coldcore.misc.scala

import java.util.regex.Pattern
import annotation.tailrec

object StringOp {

  // ************** Transforming (no dependencies) **************** //

  /** Match a first regex in a text. Return a range where a match occurs. */
  private def matchRegex(regex: String, text: String, flags: Int, start: Int = 0) = {
    val re = Pattern compile(regex, flags)
    val m = re matcher text
    if (m find start) (m.start, m.end) else (-1, -1)
  }

  /** Create an indexed token with a matched prefix and suffix regex in a text.
      Return a map with a prefix/suffix range. */
  private def indexToken(pfx: String, sfx: String, text: String, flags: Int): Option[((Int,Int),(Int,Int))] = {
    val matchedPfx = matchRegex(pfx, text, flags)
    if (matchedPfx._2 == -1) None
    else {
      val matchedSfx = matchRegex(sfx, text, flags, matchedPfx._2)
      if (matchedSfx._2 == -1) None else Some(matchedPfx, matchedSfx)
    }
  }

  /** Operate on an indexed token to extract a token value from a text laying between its prefix and suffix. */
  private def token(indexedToken: Option[((Int,Int),(Int,Int))], text: String) =
    (indexedToken: @unchecked) match {
      case Some(((_, a), (b, _))) => text substring(a, b)
    }

  /** Operate on an indexed token to extract a prefix value from a text. */
  private def tokenPfx(indexedToken: Option[((Int,Int),(Int,Int))], text: String) =
    (indexedToken: @unchecked) match {
      case Some(((a, b), _)) => text substring(a, b)
    }

  /** Operate on an indexed token to extract a suffix value from a text. */
  private def tokenSfx(indexedToken: Option[((Int,Int),(Int,Int))], text: String) =
    (indexedToken: @unchecked) match {
      case Some((_, (a, b))) => text substring(a, b)
    }

  /** Find all tokens in a text with a matched prefix and suffix.
      Return a list of tokens. */
  def findTokens(pfx: String, sfx: String, text: String, flags: Int = 0): List[String] = {
    @tailrec def next(s: String, r: List[String]): List[String] =
      indexToken(pfx, sfx, s, flags) match {
        case None => r.reverse
        case t @ Some((_, (_, i))) => next(s substring i, token(t, s) :: r)
      }
    next(text, List.empty[String])
  }

  /** Find all tokens in a text with a matched prefix and suffix (case insensitive search).
      Return a list of tokens. */
  def findTokensCI(pfx: String, sfx: String, text: String): List[String] =
    findTokens(pfx, sfx, text, Pattern.CASE_INSENSITIVE)

  /** Identify all prefixes and suffixes in a text based on regex. Return a list of prefixes and suffixes. */
  private def tokensPfxSfx(pfx: String, sfx: String, text: String, flags: Int): List[String] = {
    @tailrec def next(s: String, r: Set[String]): Set[String] =
      indexToken(pfx, sfx, s, flags) match {
        case None => r
        case t @ Some((_, (_, i))) => next(s substring i, r + tokenPfx(t, s) + tokenSfx(t, s))
      }
    next(text, Set.empty[String]).toList
  }

  /** Replace strings in a text based on a sequence containing pairs (replace a key with a value for each pair).
      abcdef [[a b] [c d] [e f]] -> bbddff */
  def chainReplace(text: String, x: Seq[(String,String)]): String =
    x.foldLeft(text)((a, b) => a.replace(b._1, b._2))

  /** Create a cross product from a map containing all combinations of prefixes and suffixes split into pairs. */
  private def crossSeq(rep: Seq[(String,String)], pfxSfx: List[String]) = {
    for {
      a <- pfxSfx.grouped(2).toList
      b <- rep
    } yield (a.head+b._1+a.last, b._2)
  }

  /** Replace tokens in a text laying between a prefix and suffix regex with values provided in a map.
      Look for a map key surrounded by a prefix and suffix and change it with a map value. */
  def replaceTokens(pfx: String, sfx: String, text: String, rep: Seq[(String,String)], flags: Int = 0): String = {
    val pfxSfx = tokensPfxSfx(pfx, sfx, text, flags)
    val repSeq = crossSeq(rep, pfxSfx)
    if (repSeq.isEmpty) text else chainReplace(text, repSeq)
  }

  /** Replace tokens as in the replace-tokens method (case insensitive prefix/suffix search). */
  def replaceTokensCI(pfx: String, sfx: String, text: String, rep: Seq[(String,String)]): String =
    replaceTokens(pfx, sfx, text, rep, Pattern.CASE_INSENSITIVE)

  /** Split a string into a key and value. Return a [key val] pair. */
  private def keyvalPair(prm: String) =
    (prm substring(0, prm indexOf ":"), prm substring(prm.indexOf(":") + 1))

  /** Substitude parameters in a text with values provided as a sequence (param:value param:value). */
  def parametrizeText(text: String, prms: Seq[String]): String =
    replaceTokensCI("\\{", "\\}", text, prms map keyvalPair)

  /** Substitude parameters in a text with provided values (param:value param:value).
      Welcome {name} name:Katie -> Welcome Katie */
  def parametrize(text: String, prms: String*): String =
    parametrizeText(text, prms.toList)

  // ************** Trimming (no dependencies) **************** //

  /** Trim and upper case a string. */
  def trimu(s: String): String =
    s.trim.toUpperCase

  /** Trim and lower case a string. */
  def triml(s: String): String =
    s.trim.toLowerCase

  /** Trim a string removing redundant line feeds. */
  def trimm(s: String): String =
    s split "\n" map (_.trim) filter (_.length > 0) mkString "\n"

  /** Trim a string and cut if it exceeds maximum lenght. */
  def trimCut(s: String, len: Int): String = {
    val s1 = s.trim
    if (s1.length > len) s1.substring(0, len).trim else s1
  }

  // ************** Parsing (depends on Transforming and Trimming) **************** //

  /** Convert a string into a number. Return a number or 0 or default is specified. */
  def toLong(s: String, d: Long = 0L): Long =
    try { s.trim.toLong } catch { case _ : Throwable => d }

  /** Convert a string into a number. Return a number or 0 or default is specified. */
  def toInt(s: String, d: Int = 0): Int =
    try { s.trim.toInt } catch { case _ : Throwable => d }

  /** Convert a string into a boolean (1 - true, 0 - false). Return a boolean or false or default is specified. */
  def isTrue(s: String, d: Boolean = false): Boolean =
    toInt(s, if (d) 1 else 0) == 1

  /** Convert a string into a sequence of pairs.
      ',:+-' -> [: ,] [+ ,] [- ,] */
  private def repSeq(sp: String): List[(String,String)] =
    sp.map(a => (a.toString, sp.head.toString)).toList

  /** Parse a comma separated values. Return a sequence of trimmed strings. Inputs: text and a separator string.
      ['foo, bar; goo' ',;' -> [foo bar goo] */
  def parseCSV(s: String, sp: String = ",;"): List[String] = {
    val s1 = chainReplace(s, repSeq(sp)) //convert text to a single separator (foo, bar; goo -> foo, bar, goo)
    val arr = s1 split(Pattern quote sp.head.toString) //split text using a separator (as regex)
    arr.map(_.trim).filter(_.length > 0).toList
  }

  /** Parse a comma separated map values. Return a map with trimmed keys and values. Inputs: text and a separator string.
      ['foo=F, bar=B; goo=G' ',;' -> {foo A, bar B, goo G} */
  def parseCSVMap(s: String, sp: String = ",;"): Map[String, String] = {
    val r =
      for {
        x <- parseCSV(s, sp)
        i = x indexOf "="
        a = x.substring(0, i).trim
        b = x.substring(i + 1).trim
      } yield a -> b
    r.toMap
  }

  /** Convert a string with precision into a whole number or a whole number back into a string with precision.
      ['123.56' 2] -> 12356   ['123.56' 4] -> 1235600
      [12356 2] -> '123.56'   [1235600 4] -> '123.56' */
  def toXPrec(s: String, prec: Int): Long = {
    val mult = scala.math.pow(10, prec).toLong
    val zero = mult.toString substring 1
    val neg = s startsWith "-"
    val pos = s startsWith "+"
    val val0 = "0" + (if (neg || pos) s substring 1 else s) replace(",", ".")
    val val1 = (if (val0.contains(".")) val0 else val0 + ".") + zero
    val arr = val1 split "\\."
    val sign= if (neg) -1L else 1L
    (arr.head.toLong * mult + arr.last.substring(0, zero.length).toLong) * sign
  }
  def toXPrec(n: Long, prec: Int): String = {
    val mult = scala.math.pow(10, prec).toLong
    val z = scala.math.abs(n)
    val a = z / mult
    val b = z - a * mult
    val d = (1 until mult.toString.length - b.toString.length).map(_ => "0").mkString
    val s = s"$a.$d$b"
    val sign = if (n < 0) "-" else ""
    sign + s
  }

  /** Convert a string or number with a precision of 2.
      ['123.56' 2] -> 12356   [12356 2] -> '123.56' */
  def toX100(x: String): Long =
    toXPrec(x, 2)
  def toX100(x: Long): String =
    toXPrec(x, 2)

  /** Convert a latitude or longiture string or number with maximum precision of 16. */
  def toXLatLng(x: String): Long =
    toXPrec(x, 16)
  def toXLatLng(x: Long): String =
    toXPrec(x, 16)

}